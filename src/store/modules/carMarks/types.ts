export interface CarModel {
    id: number;
    name: string;
}

export interface CarMark {
    id: number;
    name: string;
    items: CarModel[];
}

export enum Action {
    Add = 'add',
    Remove = 'remove'
}

export interface HistoryItem {
    id: number;
    name: string;
    action: Action;
    timestamp: Date;
}

export interface IPayload {
    item: CarMark;
    action: Action;
}


export interface CarMarksState {
    isDataFetched: boolean;
    data: CarMark[];
    actualList: CarMark[];
    addedList: CarMark[];
    historyList: HistoryItem[]
}