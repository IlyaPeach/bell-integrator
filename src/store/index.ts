import Vue from 'vue';
import Vuex, {StoreOptions} from 'vuex';
import {RootState} from "@/store/types"
import {carMarks} from './modules/carMarks/';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
    state: {},
    modules: {
        carMarks
    }
};


export default new Vuex.Store<RootState>(store);
