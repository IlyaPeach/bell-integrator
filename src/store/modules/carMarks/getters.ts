import {GetterTree} from 'vuex';
import {CarMark, CarMarksState, HistoryItem} from './types';
import {RootState} from "@/store/types";

export const getters: GetterTree<CarMarksState, RootState> = {
    getIsDataFetched(state): boolean {
        return state.isDataFetched;
    },
    getActualList(state): CarMark[] {
        return state.actualList;
    },
    getAddedList(state): CarMark[] {
        return state.addedList;
    },
    getHistoryList(state): HistoryItem[] {
        return state.historyList;
    }

};