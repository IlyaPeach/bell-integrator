import {Module} from 'vuex';
import {getters} from './getters';
import {actions} from './actions';
import {mutations} from './mutations';
import {CarMark, CarMarksState} from './types';
import {RootState} from "@/store/types";

export const state: CarMarksState = {
    isDataFetched: false,
    data: [],
    actualList: [],
    addedList: [],
    historyList: []

};


const namespaced: boolean = true;

export const carMarks: Module<CarMarksState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};