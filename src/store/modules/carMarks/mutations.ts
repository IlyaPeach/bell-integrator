import {MutationTree} from 'vuex';
import {Action, CarMark, CarMarksState, HistoryItem, IPayload} from './types';

export const mutations: MutationTree<CarMarksState> = {
    listLoaded(state, payload: CarMark[]) {
        state.data = payload;
        state.actualList = payload.slice();
        state.isDataFetched = true;
    },

    changeActualList(state, payload: IPayload) {
        if (payload.action === Action.Add) {
            let index = state.actualList.findIndex((el: CarMark) => el.id === payload.item.id);
            index !== -1 ? state.actualList.splice(index, 1) : null;
        } else if (payload.action === Action.Remove) {
            state.actualList.push(payload.item);
        }
    },

    changeAddList(state, payload: IPayload) {
        if (payload.action === Action.Add) {
            state.addedList.push(payload.item);
        } else if (payload.action === Action.Remove) {
            let index = state.addedList.findIndex((el: CarMark) => el.id === payload.item.id);
            index !== -1 ? state.addedList.splice(index, 1) : null;

        }
    },

    addToHistoryList(state, payload: HistoryItem) {
        state.historyList.push(payload)
    }
};