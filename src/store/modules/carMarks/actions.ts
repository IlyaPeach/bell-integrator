import {ActionTree} from 'vuex';
import {Action, CarMarksState, IPayload} from './types';
import {RootState} from "@/store/types";


export const actions: ActionTree<CarMarksState, RootState> = {
    fetchMarks({commit}): any {
        let request = new XMLHttpRequest();
        request.responseType = 'json';
        request.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                commit('listLoaded', this.response)
            }
        }
        // request.open('GET', 'https://run.mocky.io/v3/86229e2e-cc24-479a-8cc1-b88989957742');
        request.open('GET', 'https://api.mockaroo.com/api/dda57280?count=100&key=97c6a070');
        request.send();
    },
    moveItem: function ({commit}, payload: IPayload): any {
        commit('changeAddList', payload);
        commit('changeActualList', payload);
        commit('addToHistoryList', {
            id: payload.item.id,
            name: payload.item.name,
            action: payload.action,
            date: new Date()
        });

    },
}